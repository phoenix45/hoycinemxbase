<div class="row">
    <div class="col-xs-12 column">
        <div data-ride="carousel" class="carousel slide" id="myCarousel">
            <ol class="carousel-indicators">
                <li class="active" data-slide-to="0" data-target="#myCarousel"></li>
                <li data-slide-to="1" data-target="#myCarousel" class=""></li>
                <li data-slide-to="2" data-target="#myCarousel" class=""></li>
            </ol>
            <div role="listbox" class="carousel-inner">
                <div class="item active">
                    <img alt="First slide" src="img/picture-1.png">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Example headline.</h1>
                            <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
                            <p><a role="button" href="#" class="btn btn-lg btn-primary">Sign up today</a></p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img alt="Second slide" src="img/picture-1.png">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Another example headline.</h1>
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                            <p><a role="button" href="#" class="btn btn-lg btn-primary">Learn more</a></p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img alt="Third slide" src="img/picture-1.png">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>One more for good measure.</h1>
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                            <p><a role="button" href="#" class="btn btn-lg btn-primary">Browse gallery</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <a data-slide="prev" role="button" href="#myCarousel" class="left carousel-control">
                <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a data-slide="next" role="button" href="#myCarousel" class="right carousel-control">
                <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>