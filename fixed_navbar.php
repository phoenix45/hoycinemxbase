<div class="navbar navbar-custom navbar-inverse navbar-static-top" id="nav">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div role="navigation">
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav nav-justified">
                        <li class="active"><a href="#"><i class="fa fa-film"></i> Cartelera</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Cines y espacios alternativos</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-fire" aria-hidden="true"></span> Eventos de Cine</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> Mis Favoritos</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
