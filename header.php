<!doctype html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <title>Hoy Cine</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Hoy Cine MX">
    <link href="css/bootstrap-stylus/dist/bootstrap.css" rel="stylesheet">
    <?php /* Add enqueue metod */ ?>
    <link href="css/style5.css" rel="stylesheet">
    <?php /*<link href="css/debug.css" rel="stylesheet">*/ ?>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->
    <?php /*Fav and touch icons*/ ?>
    <link rel="Hoy Cine mx" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
    <link rel="Hoy Cine mx" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
    <link rel="Hoy Cine mx" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
    <link rel="Hoy Cine mx" href="img/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="img/favicon.png">
    <script type="text/javascript" src="js/jquery.min.js" async></script>
    <script type="text/javascript" src="js/bootstrap.min.js" async></script>
    <link rel="stylesheet" href="css/overlay.css" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300ita‌​lic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>
    <!--script type="text/javascript" src="js/scripts.js" async></script-->
</head>
<body> <?php /*Add open tag <body>*/ ?>
<header>

    <?php /* <div class=".cover-container ">*/ ?>
    <nav class="container-head container-fluid">
        <?php /*<div class="row clearfix">*/ ?>
        <div class="row">
            <?php /* Ini Logo */ ?>
            <!-- <div class="col-xs-2 column logo col-xs-height col-middle"> -->
            <div class="col-xs-2 column logo col-xs-height col-middle">
                <img alt="logo hoy cine mx" src="img/hoycinemx.png" class="item">
            </div>
            <?php /* End Logo */ ?>

            <?php /* City */ ?>
            <!-- <div class="col-xs-2 column logo col-xs-height col-middle"> -->
            <div class="col-xs-2 column col-xs-height col-middle">
                <div class="item">
                    <div class="content">
                        <div class="text-center">
                            <div class="dropdown ciudades" data-toggle="dropdown">
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">CIUDAD DE MÉXICO</a></li>
                                    <li><a href="#">GUADALAJARA</a></li>
                                </ul>
                                <label class="dropdown-toggle gris" data-toggle="dropdown">YUCATÁN</label>
                                <i class="fa fa-angle-down" data-toggle="dropdown"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php /* End City */ ?>

            <?php /* Date */ ?>
            <!-- <div class="col-xs-2 column logo col-xs-height col-middle"> -->
            <div class="col-xs-3 column col-xs-height col-middle">
                <div class="item">
                    <div class="content content2">
                    <?php /*Cambio a lineas para que se vea mas limpio*/ ?>
                        <p>
                            <span class="naranja">87</span> Películas en cartelera
                            | 
                            <span class="naranja">24</span> cines
                        </p>
                    </div>
                </div>
            </div>
            <?php /* End Date */ ?>

            <?php /* Empty column */  ?>
            <!-- <div class="col-xs-2 column logo col-xs-height col-middle"> -->
            <div class="col-xs-2 column col-xs-height col-middle">
            </div>
            <?php /* End Empty Column */ ?>


            <?php /* Login form */ ?>
            <!-- <div class="col-xs-2 column logo col-xs-height col-middle"> -->
            <div class="col-xs-2 column text-right col-xs-height">
                <!-- Large modal -->
                <div class="item item2">
                    <div class="content">
                        <button type="button" class="btn-oldstyle btn-login pull-right" data-toggle="modal" data-target=".bs-example-modal-lg">
                            <div class="usuario linea"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
                            <span>Registrate o <br />inicia sesión</span></button>
                    </div>
                </div>
                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                <form role="form">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Usuario:</label>
                                        <input type="text" class="form-control" id="recipient-name">
                                    </div>
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Contraseña:</label>
                                        <input type="text" class="form-control" id="recipient-name">
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="button" class="btn btn-primary">Entrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php /* End login form */ ?>

        </div>
    </nav>
</header>
<?php /*End Header*/ ?>