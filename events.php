<div class="row">
        <div class="col-xs-12 column
            <div class="well">
                <div class="eventos">
                    Eventos
                </div>
                <div class="festivales">
                    Festivales, ciclos, talleres y más.
                </div>
                <div id="myCarousel2" class="carousel fdi-Carousel slide">
                    <!-- Carousel items -->
                    <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                        <div class="carousel-inner onebyone-carosel">
                            <div class="item active">
                                <div class="col-xs-4">
                                    <a href="#"><img src="img/350x150.gif" class="img-responsive center-block"></a>
                                    <div class="texto">
                                        <div class="pie-de-foto-titulo">
                                            El horror que vino de Oriente (Japón)
                                        </div>
                                        <div class="pie-de-foto">
                                            <div class="lugar">
                                                cineta nacional
                                            </div>
                                            <div class="tiempo">
                                                del 20 al 31 de agosto del 2014
                                            </div>
                                            <div class="informacion-extra">
                                                20 hrs. Entrada libre.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-4">
                                    <a href="#"><img src="img/350x150.gif" class="img-responsive center-block"></a>
                                    <div class="texto">
                                        <div class="pie-de-foto-titulo">
                                            El horror que vino de Oriente (Japón)
                                        </div>
                                        <div class="pie-de-foto">
                                            <div class="lugar">
                                                cineta nacional
                                            </div>
                                            <div class="tiempo">
                                                del 20 al 31 de agosto del 2014
                                            </div>
                                            <div class="informacion-extra">
                                                20 hrs. Entrada libre.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-4">
                                    <a href="#"><img src="img/350x150.gif" class="img-responsive center-block"></a>
                                    <div class="texto">
                                        <div class="pie-de-foto-titulo">
                                            El horror que vino de Oriente (Japón)
                                        </div>
                                        <div class="pie-de-foto">
                                            <div class="lugar">
                                                cineta nacional
                                            </div>
                                            <div class="tiempo">
                                                del 20 al 31 de agosto del 2014
                                            </div>
                                            <div class="informacion-extra">
                                                20 hrs. Entrada libre.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-4">
                                    <a href="#"><img src="img/350x150.gif" class="img-responsive center-block"></a>
                                    <div class="texto">
                                        <div class="pie-de-foto-titulo">
                                            El horror que vino de Oriente (Japón)
                                        </div>
                                        <div class="pie-de-foto">
                                            <div class="lugar">
                                                cineta nacional
                                            </div>
                                            <div class="tiempo">
                                                del 20 al 31 de agosto del 2014
                                            </div>
                                            <div class="informacion-extra">
                                                20 hrs. Entrada libre.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-4">
                                    <a href="#"><img src="img/350x150.gif" class="img-responsive center-block"></a>
                                    <div class="texto">
                                        <div class="pie-de-foto-titulo">
                                            El horror que vino de Oriente (Japón)
                                        </div>
                                        <div class="pie-de-foto">
                                            <div class="lugar">
                                                cineta nacional
                                            </div>
                                            <div class="tiempo">
                                                del 20 al 31 de agosto del 2014
                                            </div>
                                            <div class="informacion-extra">
                                                20 hrs. Entrada libre.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-4">
                                    <a href="#"><img src="img/350x150.gif" class="img-responsive center-block"></a>
                                    <div class="texto">
                                        <div class="pie-de-foto-titulo">
                                            El horror que vino de Oriente (Japón)
                                        </div>
                                        <div class="pie-de-foto">
                                            <div class="lugar">
                                                cineta nacional
                                            </div>
                                            <div class="tiempo">
                                                del 20 al 31 de agosto del 2014
                                            </div>
                                            <div class="informacion-extra">
                                                20 hrs. Entrada libre.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="left lefti carousel-control" href="#eventCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                        <a class="right righti carousel-control" href="#eventCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
                    </div>
                    <!--/carousel-inner-->
                </div>
                <!--/myCarousel-->
            </div>
            <!--/well-->
        </div>
    </div>