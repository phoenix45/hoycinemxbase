<?php 
include 'header.php'; 
include 'fixed_navbar.php'; // Fixed navbar 
include 'slide_indicator.php' // Indicators Slide. Si se elimina esta opción desaparece el slide
?>
<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-12 col-sm-9">
            <?php 
            include 'date_under_slide.php'; // Date under slide (list)
            include 'films.php';// Films boxes
            include 'opinometro.php' // Opinometro  
            ?>
        </div>
        <?php include 'sidebar.php';// For Sidebar ?>
    </div>
    <?php
    include 'events.php'; // Eventos 
    include 'pub.php';// Publicidad 950 x 100 
    include 'sponsors.php';//Sponsors 
    include 'footer.php' // Footer 
?>
