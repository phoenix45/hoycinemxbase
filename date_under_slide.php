<div class="espacio">
    <div role="navigation">
        <ul class="nav nav-justified">
            <li class="active dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lunes 18 de Agosto <span class="right">  <i class="fa fa-angle-down fa-2x" data-toggle="dropdown"></i></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#">Martes 19 de Agosto</a>
                    </li>
                    <li>
                        <a href="#">Miercoles 20 de Agosto</a>
                    </li>
                    <li>
                        <a href="#">Jueves 21 de Agosto</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"> <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Próximamente</a>
            </li>
        </ul>
    </div>
</div>