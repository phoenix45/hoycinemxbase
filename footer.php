<footer>
    <!-- <div class="container"> -->
        <div class="row clearfix">
            <div class="col-xs-2 column logo col-xs-height col-middle">
                <img alt="logo hoy cine mx" src="img/hoycinemx.png" class="item center-block">
            </div>
            <div class="col-xs-2 column col-xs-height col-middle">
                <div class="item">
                    <div class="content">
                        <div class="texto-footer">
                            Un proyecto de <span class="diho">.diho.</span>
                            <br /> Cel.9991137231
                            <br /> &copy; 2014 Todos los
                            <br /> Derechos Reservados
                        </div>
                        <div class="social2">
                            <a class="fa fa-facebook" title="" target="_blank" href="#"></a>
                            <a class="fa fa-twitter" title="" target="_blank" href="#"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-2 column logo col-xs-height col-middle">
                <div class="item">
                    <div class="content">
                        <ul class="menu-footer">
                        <!-- La etiqueta de cierre li y a estaban entrecruzaas -->
                        <li><a href="#">-Quienes somos</a></li>
                        <li><a href="#">-Aviso de privacidad</a></li>
                        <li><a href="#">-¿Quieres anunciarte?</a></li>
                        <li><a href="#">-Términos y condiciones</a></li>
                        <li><a href="#">-Mapa de sitio</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-1 column logo col-xs-height col-middle">
                <div class="item">
                    <div class="content">
                        <a href="#"><img src="img/350x150.gif" class="img-responsive center-block"></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-5 column logo col-xs-height col-middle">
                <div class="item">
                    <div class="content">
                        <a href="#"><img src="img/350x150.gif" class="img-responsive center-block"></a>
                    </div>
                </div>
            </div>
        </div>
</footer>
</div>
<?php /*End container*/ ?>
<script>
$(document).ready(function() {

    $("#myCarousel").carousel();
    /**/
    $('#myCarousel2, #myCarousel3').carousel({
        interval: 3000
    })
    $('.fdi-Carousel .item').each(function() {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length > 0) {
            next.next().children(':first-child').clone().appendTo($(this));
        } else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });




});
$('#nav').affix({
    offset: {
        top: $('header').height() - $('#nav').height()
    }
});
/* highlight the top nav as scrolling occurs */
$('body').scrollspy({
    target: '#nav'
})
</script>
</body>

</html>